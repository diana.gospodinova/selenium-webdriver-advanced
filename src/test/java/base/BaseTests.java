package base;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.HomePage;

public class BaseTests {
    private static WebDriver driver;
    protected static HomePage homePage;

    @BeforeClass
    public static void beforeAllTestMethods() {
        System.setProperty("webdriver.chrome.driver", "resources/chromedriver.exe");
        driver = new ChromeDriver(getChromeOptions());
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
    }

    @Before
    public void setUp() {
        driver.get("https://the-internet.herokuapp.com/");
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    private static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();

        //Headless mode
        // options.setHeadless(true);
        return options;
    }
}
