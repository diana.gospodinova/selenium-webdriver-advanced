package login;

import base.BaseTests;
import org.junit.Test;
import pages.LoginPage;
import pages.SecureAreaPage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;


public class LoginPageTests extends BaseTests {
    @Test
    public void testSuccessfulLogin() {
        // Arrange
        String username = "tomsmith";
        String password = "SuperSecretPassword!";
        String expectedAlertText = "You logged into a secure area!";
        LoginPage loginPage = homePage.clickFormAuthentication();
        loginPage.setUsername(username);
        loginPage.setPassword(password);
        // Act
        SecureAreaPage secureAreaPage = loginPage.clickLoginButton();
        // Assert
        assertTrue("Alert text doesn't match",secureAreaPage.getAlertText().contains(expectedAlertText));
        /**
         * The example below implements "Soft Assert". The feature is not supported by JUnit so we need to use an
         * external library as AssertJ
         */
        assertThat(secureAreaPage.getAlertText().contains(expectedAlertText)).isTrue();
    }
}
