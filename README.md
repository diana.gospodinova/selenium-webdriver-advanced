Test Automation project with Selenium WebDriver, Java and JUnit.\
Design pattern: Page Object Model\
Test application: https://the-internet.herokuapp.com/ \
WebDriver: Chrome (available in resources folder)\
Covered features:
 - Hard and soft assertions - LoginTests@testSuccessfulLogin
 - Page Factory pattern - LoginPage@usernameFieldPageFactory